class BookModel {
  final String title;
  final String author;
  final String cover;

  BookModel(this.title, this.author, this.cover);
}
