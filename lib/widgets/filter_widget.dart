import 'package:digital_book/controller/filter_controller.dart';
import 'package:digital_book/models/items_filter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FilterCategoriesWidget extends StatelessWidget {
  final bool isSelected;
  final ItemsFilter filter;

  const FilterCategoriesWidget(
      {Key? key, required this.isSelected, required this.filter})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FilterController>(builder: (controller) {
      return Center(
        child: Container(
          margin: const EdgeInsets.only(left: 15),
          height: 45,
          padding: const EdgeInsets.only(left: 20, right: 20),
          decoration: BoxDecoration(
              color: isSelected ? Colors.orangeAccent : Colors.grey.withOpacity(0.1),
              borderRadius: BorderRadius.circular(25),
          ),
          child: Center(
            child: Text(filter.name,
                style: TextStyle(
                    color: isSelected ? Colors.white : Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.w500)),
          ),
        ),
      );
    });
  }
}
