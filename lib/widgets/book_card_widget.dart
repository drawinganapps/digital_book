import 'package:digital_book/models/book_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BookCardWidget extends StatelessWidget {
  final BookModel book;
  const BookCardWidget({Key? key, required this.book}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    String convertBookTitle(String value){
      if (value.length > 15) {
        return value.substring(0, 13) + '...';
      }
      return value;
    }

    return Container(
      margin: const EdgeInsets.only(left: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            clipBehavior: Clip.antiAlias,
            child: Image.asset('assets/img/${book.cover}', width: 150, height: 230, fit: BoxFit.cover),
          ),
          Container(
            padding: const EdgeInsets.only(top: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(convertBookTitle(book.title), style: const TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 20
                )),
                Text(book.author, style: const TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 18,
                  color: Colors.grey
                )),
              ],
            ),
          )
        ],
      ),
    );
  }
}
