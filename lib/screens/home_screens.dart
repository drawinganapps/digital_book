import 'package:digital_book/pages/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class HomeScreens extends StatelessWidget {
  const HomeScreens({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
   return Scaffold(
       body: Column(
         crossAxisAlignment: CrossAxisAlignment.start,
         children: const [
           HomePage()
         ],
       ),
       bottomNavigationBar: Container(
         decoration: BoxDecoration(
           color: Colors.grey.withOpacity(0.08),
         ),
         height: 80,
         child: Row(
           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
           children: [
             IconButton(
               enableFeedback: false,
               onPressed: () {},
               icon: const Icon(
                 Icons.home_rounded,
                 size: 30,
               ),
             ),
             IconButton(
               enableFeedback: false,
               onPressed: () {},
               icon: Icon(Icons.video_library_outlined, color: Colors.grey.withOpacity(0.8)),
             ),
             IconButton(
               enableFeedback: false,
               onPressed: () {},
               icon: Icon(Icons.auto_stories_outlined, color: Colors.grey.withOpacity(0.5)),
             ),
             IconButton(
               enableFeedback: false,
               onPressed: () {},
               icon: Icon(Icons.person_outline_outlined, color: Colors.grey.withOpacity(0.5)),
             ),
           ],
         ),
       ));
  }

}
