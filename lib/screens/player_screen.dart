import 'package:digital_book/screens/home_screens.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class PlayerScreen extends StatelessWidget {
  const PlayerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            Flexible(
              child: ListView(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 20),
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: () {
                              Get.to(const HomeScreens());
                            }, icon: const Icon(Icons.arrow_back)),
                        IconButton(
                            onPressed: () {}, icon: const Icon(Icons.more_horiz)),
                      ],
                    ),
                  ),
                  Center(
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      clipBehavior: Clip.antiAlias,
                      child: Image.asset('assets/img/book5.jpg',
                          width: 250, height: 350, fit: BoxFit.fill),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Column(
                      children: const [
                        Text('The Girl and the Moon',
                            style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.bold,
                            ),
                            textAlign: TextAlign.center),
                        Text(
                          'Chapter 3 - The Third Book Of the Ice',
                          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                          textAlign: TextAlign.center,
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.08, right: MediaQuery.of(context).size.width * 0.08),
                    margin: const EdgeInsets.only(top: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        IconButton(
                            onPressed: () {},
                            iconSize: 30,
                            icon: const Icon(Icons.skip_previous)),
                        IconButton(
                            onPressed: () {},
                            iconSize: 40,
                            icon: const Icon(Icons.replay_10)),
                        IconButton(
                            onPressed: () {},
                            iconSize: 75,
                            color: Colors.orange,
                            icon: const Icon(Icons.play_circle_filled)),
                        IconButton(
                            onPressed: () {},
                            iconSize: 40,
                            icon: const Icon(Icons.forward_10)),
                        IconButton(
                            onPressed: () {},
                            iconSize: 30,
                            icon: const Icon(Icons.skip_next)),
                      ],
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 50, right: 50),
                    margin: const EdgeInsets.only(top: 15),
                    child: Column(
                      children: [
                        LinearPercentIndicator(
                          lineHeight: 8.0,
                          percent: 0.35,
                          progressColor: Colors.orange,
                          backgroundColor: Colors.grey.withOpacity(0.2),
                          animation: true,
                          barRadius: const Radius.circular(50),
                          padding: const EdgeInsets.all(0),
                        ),
                        Container(
                          margin: const EdgeInsets.only(bottom: 5),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: const [
                            Text('11:10',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15)),
                            Text('30:00',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 15)),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            )
          ],
        ),
        bottomNavigationBar: SizedBox(
          height: 80,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                children: const [
                  Icon(Icons.keyboard_arrow_up, size: 35),
                  Text('Chapters', style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w600
                  ))
                ],
              )
            ],
          ),
        ));
  }
}
