import 'package:digital_book/models/book_model.dart';
import 'package:digital_book/models/items_filter.dart';

class Dummy {
  static List<ItemsFilter> filterList = <ItemsFilter>[
    const ItemsFilter('Fantasy', 'FANTASY'),
    const ItemsFilter('History', 'HISTORY'),
    const ItemsFilter('Science & Fiction', 'SCIENCE_FICTION'),
    const ItemsFilter('Romantic', 'ROMANTIC'),
  ];

  static List<BookModel> books = [
    BookModel('The Midnight Library', 'Matt Haig', 'book1.jpg'),
    BookModel('A Court of Thorns and Roses', 'Sarah J. Maas', 'book2.jpg'),
    BookModel('Immortal Rising', 'Lynsay Sands', 'book3.jpg'),
    BookModel('House of Sky and Breath', 'Sarah J. Maas', 'book4.jpg'),
    BookModel('The Girl and the Moon', 'Mark Lawrence', 'book5.jpg'),
  ];
}
