import 'package:digital_book/controller/filter_controller.dart';
import 'package:digital_book/data/dummy.dart';
import 'package:digital_book/models/book_model.dart';
import 'package:digital_book/models/items_filter.dart';
import 'package:digital_book/screens/player_screen.dart';
import 'package:digital_book/widgets/book_card_widget.dart';
import 'package:digital_book/widgets/filter_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<ItemsFilter> filterList = Dummy.filterList;
    final List<BookModel> books = Dummy.books;
    return GetBuilder<FilterController>(builder: (controller) {
      return Flexible(
        child: ListView(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(bottom: 5),
                        child: Text('Good Day,',
                            style: TextStyle(
                                color: Colors.black.withOpacity(0.8),
                                fontSize: 30,
                                fontWeight: FontWeight.w600)),
                      ),
                      Text('Sarah!',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 30,
                              color: Colors.black.withOpacity(0.8))),
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(50)),
                    clipBehavior: Clip.hardEdge,
                    child: Image.asset('assets/img/profile.jpg',
                        width: 50, height: 50, fit: BoxFit.cover),
                  ),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(15),
              margin: const EdgeInsets.only(
                  left: 15, right: 15, top: 30, bottom: 10),
              decoration: BoxDecoration(
                color: Colors.white70,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.3),
                    spreadRadius: 1,
                    blurRadius: 10,
                  ),
                ],
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text('Weekly goal',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20)),
                      Row(
                        children: const [
                          Text('210',
                              style: TextStyle(
                                  fontWeight: FontWeight.w700, fontSize: 18)),
                          Text('/300 minutes', style: TextStyle(fontSize: 18)),
                        ],
                      )
                    ],
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10, bottom: 10),
                    child: LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width - 60,
                      lineHeight: 8.0,
                      percent: 0.75,
                      progressColor: Colors.orange,
                      backgroundColor: Colors.grey.withOpacity(0.2),
                      animation: true,
                      barRadius: const Radius.circular(50),
                      padding: const EdgeInsets.all(0),
                    ),
                  ),
                  const Text('Fast reading, keep it up!',
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 16))
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15),
              margin: EdgeInsets.only(top: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text('For you',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 22)),
                  TextButton(
                      onPressed: () {},
                      child: const Text('See all',
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.grey,
                              fontWeight: FontWeight.w600)))
                ],
              ),
            ),
            Container(
                height: 50,
                margin: const EdgeInsets.only(bottom: 30),
                child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: List.generate(filterList.length, (index) {
                      return GestureDetector(
                        child: FilterCategoriesWidget(
                            isSelected: index == controller.selectedFilter,
                            filter: filterList[index]),
                        onTap: () {
                          controller.changeFilter(index);
                        },
                      );
                    }))),
            SizedBox(
                height: 300,
                child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: List.generate(books.length, (index) {
                      return GestureDetector(
                        child: BookCardWidget(
                          book: books[index],
                        ),
                      );
                    }))),
            Container(
              padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        margin: const EdgeInsets.only(right: 10),
                        clipBehavior: Clip.antiAlias,
                        child: Image.asset('assets/img/book5.jpg',
                            height: 60, width: 60, fit: BoxFit.cover),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('The Girl and the...',
                              style: TextStyle(
                                  fontSize: MediaQuery.of(context).size.width * 0.06, fontWeight: FontWeight.bold)),
                          const Text('11:10',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18)),
                        ],
                      )
                    ],
                  ),
                  IconButton(
                      onPressed: () {
                        Get.to(const PlayerScreen());
                      },
                      iconSize: 65,
                      icon: const Icon(Icons.play_circle_filled,
                          color: Colors.orange))
                ],
              ),
            )
          ],
        ),
      );
    });
  }
}
