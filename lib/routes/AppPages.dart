import 'package:digital_book/controller/filter_binding.dart';
import 'package:digital_book/screens/home_screens.dart';
import 'package:digital_book/screens/player_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'AppRoutes.dart';

class AppPages {
  static var list = [
    GetPage(
        name: AppRoutes.DASHBOARD,
        page: () => const HomeScreens(),
        binding: FilterBinding(),
        transition: Transition.rightToLeft),
    GetPage(
        name: AppRoutes.PLAYER,
        page: () => const PlayerScreen(),
        transition: Transition.rightToLeft)
  ];
}
